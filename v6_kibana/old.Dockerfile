FROM docker.elastic.co/kibana/kibana:6.7.1
COPY ./search-guard-kibana-plugin-6.7.1-18.5.zip /usr/share/kibana/
COPY ./kibana.yml /usr/share/kibana/config/kibana.yml
RUN bin/kibana-plugin install file:///usr/share/kibana/search-guard-kibana-plugin-6.7.1-18.5.zip
