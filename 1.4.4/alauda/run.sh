#!/bin/bash

if [ -z $ALAUDA_ES_USERNAME ];then
    ALAUDA_ES_USERNAME=alauda
fi

if [ -z $ALAUDA_ES_PASSWORD ];then
    ALAUDA_ES_PASSWORD=alauda
fi

if [ -z $ALAUDA_ES_CLUSTERS ];then
    ALAUDA_ES_CLUSTERS=127.0.0.1
fi


es_cluster_array=(${ALAUDA_ES_CLUSTERS//,/ })
length=${#es_cluster_array[@]}
EXPORTER=""
UNICAST=""
for ((i=0;i<length;i++))
{
   EXPORTER=${EXPORTER}"",\""$ALAUDA_ES_USERNAME:$ALAUDA_ES_PASSWORD@${es_cluster_array[$i]}"\"
   UNICAST=${UNICAST}"",\""${es_cluster_array[i]}"\"
}

EXPORTER=${EXPORTER:1}
UNICAST=${UNICAST:1}
sed -i "s/USERNAME/$ALAUDA_ES_USERNAME/g" /es/config/elasticsearch.yml
sed -i "s/PASSWORD/$ALAUDA_ES_PASSWORD/g" /es/config/elasticsearch.yml
sed -i "s/EXPORTER/$EXPORTER/g" /es/config/elasticsearch.yml
sed -i "s/UNICAST/$UNICAST/g" /es/config/elasticsearch.yml


if [ -z $ALAUDA_ES_TTL ];then
    ALAUDA_ES_TTL="7d"
fi

if [ -z $ALAUDA_ES_SHARDING ];then
    ALAUDA_ES_SHARDING=`expr $length \* 2`
fi


if [ -z $ALAUDA_ES_REPLICA ];then
    ALAUDA_ES_REPLICA="1"
fi

sed -i "s/7d/$ALAUDA_ES_TTL/g" /es/config/templates/*.json

printenv >> /etc/environment
cron

sed -i "s/ALAUDA_ES_REPLICA/$ALAUDA_ES_REPLICA/g" /es/config/templates/*.json

sed -i "s/ALAUDA_ES_SHARDING/$ALAUDA_ES_SHARDING/g" /es/config/templates/*.json


elasticsearch --path.conf=/es/config --path.data=/esdata
