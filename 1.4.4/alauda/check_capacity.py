#!/usr/bin/env python
import time
import datetime
import os
from elasticsearch import Elasticsearch
from elasticsearch.client import NodesClient
from elasticsearch.client import IndicesClient

MAX_USED_PERCENT = float(os.getenv("MAX_USED_PERCENT", -1))
MAX_USED_SIZE = int(os.getenv("MAX_USED_SIZE", -1)) * (1024 * 1024 * 1024)
ES_HOST = "127.0.0.1:9200"
ES_USERNAME = os.getenv("ALAUDA_ES_USERNAME")
ES_PASSWORD = os.getenv("ALAUDA_ES_PASSWORD")
ES_DATA_PATH = "/esdata"

if ES_USERNAME or ES_PASSWORD:
    es = Elasticsearch([ES_HOST], http_auth=(ES_USERNAME, ES_PASSWORD))
else:
    es = Elasticsearch([ES_HOST])


def get_used_size():
    used_size = 0
    for root, dirs, files in os.walk(ES_DATA_PATH):
        for f in files:
            used_size += os.path.getsize(os.path.join(root, f))

    return used_size


def get_total_size():
    node_client = NodesClient(es)
    resp = node_client.stats("_local")
    node_value = resp['nodes'].values()[0]
    total_size = node_value["fs"]["total"]["total_in_bytes"]
    return total_size


def judge_over_flow():
    used_size = get_used_size()
    total_size = get_total_size()
    over_flow = False
    if used_size >= MAX_USED_SIZE >= 0:
        over_flow = True
    if float(used_size) / total_size * 100 >= MAX_USED_PERCENT >= 0:
        over_flow = True
    return over_flow


def get_index_name(index_type, date):
    if index_type == "marvel":
        return '.marvel-%04d.%02d.%02d' % (date.year, date.month, date.day)
    elif index_type == "log":
        return 'log-%04d%02d%02d' % (date.year, date.month, date.day)


def delete_indices(index_type, date):
    index_name = get_index_name(index_type, date)
    idx_client = IndicesClient(es)
    if not idx_client.exists(index_name):
        return
    idx_client.delete(index=index_name)
    time.sleep(3)


def main():
    idx_client = IndicesClient(es)
    resp = idx_client.stats()
    index_list = resp['indices'].keys()
    log_index_list = filter(lambda x: x.startswith("log-"), index_list)
    first_index = sorted(log_index_list)[0]

    year, month, day = int(first_index[4:8]), int(first_index[8:10]), int(first_index[10:12]),
    temp_date = datetime.date(year=year, month=month, day=day)

    now_date = datetime.date.today()

    while temp_date <= now_date and judge_over_flow():
        delete_indices("log", temp_date)
        temp_date += datetime.timedelta(days=1)


if __name__ == '__main__':
    main()
