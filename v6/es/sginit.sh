#!/bin/sh

echo ">>>>  Right before SG initialization <<<<"
# use while loop to check if elasticsearch is running
while true
do
    netstat -uplnt | grep :9200 | grep LISTEN > /dev/null
    verifier=$?
    if [ 0 = $verifier ]
        then
            echo "Running search guard plugin initialization"
            # plugins/search-guard-6/tools/sgadmin.sh -cd ./plugins/search-guard-6/sgconfig/ -icl -nhnv -cacert ./config/root-ca.pem -cert ./config/kirk.pem -key ./config/kirk.key
            plugins/search-guard-6/tools/sgadmin.sh -ts /usr/share/elasticsearch/config/truststore.jks -tspass alauda_ts -ks ${SG_PATH}/sgconfig/sgadmin-keystore.jks -kspass alauda_ts -cd ${SG_PATH}/sgconfig/ -icl -nhnv
            break
        else
            echo "ES is not running yet"
            sleep 5
    fi
done
sleep 5
after_build/create_mapping.sh & 2>&1