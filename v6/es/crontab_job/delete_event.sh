#!/bin/bash

#初始化变量默认值
ttl=3d
es_master_ip=nul
es_username="alauda"
es_passwd="alauda"

#获得账户
es_username=$(cat /etc/pass_es/username)

#获得密码
es_passwd=$(cat /etc/pass_es/password)

echo 'es_username:'$es_username
echo 'es_passwd:'$es_passwd

echo '------------------------'
#获得容器ttl
ttl=`python /usr/share/elasticsearch/crontab_job/get_ttl_config.py EVENT_TTL`
echo 'ttl:'$ttl
echo '---------------------------'
#ttl必须大于等于1天
if [ $ttl -lt 1 ];then
   echo 'ttl must be equal or greater than 1 day'
   exit 0
fi

#获得es的master ip
es_master_ip=`eval curl -s -u $es_username:$es_passwd localhost:9200/_cat/master | awk '{print $3}'`
echo 'es_master_ip:'$es_master_ip
echo '------------------------'

# 列出索引
indices_event=`curl -s -u $es_username:$es_passwd $es_master_ip:9200/_cat/indices | grep event | awk '{print $3}'`


#处理日志 哪些保留 哪些删除
##列出日志索引
array_indices=($indices_event)

for i in ${array_indices[@]};
do
   echo 'event_array_indices:'$i
done
echo '------------------'
##从array_indices数组里剔除需要保留的log 剩下的被删除
for (( j=0; j<=$ttl; j++ ))
do
  param="$j days ago"
  tmp=`date -d "$param" +%Y%m%d`
  persist_event="event-"$tmp
  echo 'persist_event:'$persist_event
  for (( i=0; i<=${#array_indices[@]}; i++ ))
  do
    if [ "${array_indices[$i]}" == "$persist_event" ]; then
        array_indices[$i]=0
    fi
  done
done

for k in ${array_indices[*]};
do
  if [ $k != 0 ]; then
        echo 'need delete event:'$k
	curl -XDELETE -u $es_username:$es_passwd $es_master_ip:9200/$k
  fi
done