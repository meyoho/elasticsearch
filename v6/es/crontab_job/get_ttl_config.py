import sys
import argparse
import os
from kubernetes import client, config
from kubernetes.client.rest import ApiException

# Configs can be set in Configuration class directly or using helper utility
def get_elasticsearch_ttl(category="LOG_TTL"):
  config.load_incluster_config()
  api_instance = client.CustomObjectsApi()
  group = 'aiops.alauda.io' # str | the custom resource's group
  version = 'v1beta1' # str | the custom resource's version
  plural = 'logs' # str | the custom object's plural name. For TPRs this would be lowercase plural kind.
  name = 'alauda-es-config' # str | the custom object's name
  namespace = os.getenv("COMMON_CONFIG_NAMESPACE", "alauda-system")
  try:
    api_response = api_instance.get_namespaced_custom_object(group, version, namespace, plural, name)
    print(api_response.get('spec').get(category, "7"))
  except ApiException as e:
    print(7)

# the first param should be category(LOG_TTL, EVENT_TTL, AUDIT_TTL)
if __name__== "__main__":
  parser = argparse.ArgumentParser(description='get ttl config by category.')
  parser.add_argument('category', help='category which you what to get ttl config')
  args = parser.parse_args()
  get_elasticsearch_ttl(sys.argv[1])