#!/bin/bash

curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-log -d @./templates/log.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-resources -d @./templates/resources.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-event -d @./templates/event.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-audit -d @./templates/audit.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-meter -d @./templates/meter.json