#! /bin/bash
set -e

if [ "${1:0:1}" = '-' ]; then
	set -- elasticsearch "$@"
fi

if [ -e "/etc/es/config/elasticsearch.yaml" ]; then
    cp -f /etc/es/config/elasticsearch.yaml ./config/elasticsearch.yml
fi

#获得账户
ALAUDA_ES_USERNAME=$(cat /etc/pass_es/username)

#获得密码
ALAUDA_ES_PASSWORD=$(cat /etc/pass_es/password)

HASH_ES_PASSWORD=$(./plugins/search-guard-6/tools/hash.sh -p $ALAUDA_ES_PASSWORD)

if [ -z $ALAUDA_ES_CLUSTERS ];then
    ALAUDA_ES_CLUSTERS=127.0.0.1
fi

NODE_MASTER="true"
NODE_DATA="true"
if [[ $NODE_MODEL = "master" ]];then
    NODE_DATA="false"
fi

if [[ $NODE_MODEL = "data" ]];then
    NODE_MASTER="false"
fi

if [[ $NODE_MODEL = "none" ]];then
    NODE_MASTER="false"
    NODE_DATA="false"
fi

es_cluster_array=(${ALAUDA_ES_CLUSTERS//,/ })
length=${#es_cluster_array[@]}
EXPORTER=""
UNICAST=""
for ((i=0;i<length;i++))
{
    EXPORTER=${EXPORTER}"",\""root:$ALAUDA_ES_PASSWORD@${es_cluster_array[$i]}"\"
    UNICAST=${UNICAST}"",\""${es_cluster_array[i]}"\"
}


EXPORTER=${EXPORTER:1}
UNICAST=${UNICAST:1}

if [ -z $ALAUDA_ES_SHARDING ];then
    ALAUDA_ES_SHARDING=5
fi

if [ -z $ALAUDA_ES_REPLICA ];then
    ALAUDA_ES_REPLICA=1
fi

if [ -z $ALAUDA_ES_EXPECTED_NODE ];then
    ALAUDA_ES_EXPECTED_NODE=2
fi

if [ -z $ALAUDA_ES_MASTER_NODE ];then
    ALAUDA_ES_MASTER_NODE=$length
fi


RECOVER_AFTER_NODES=$[$[$ALAUDA_ES_EXPECTED_NODE * 2] / 3]

if [ $RECOVER_AFTER_NODES -eq 0 ];then
    RECOVER_AFTER_NODES=1
fi

MINIMUM_MASTER_NODES=`expr $[$ALAUDA_ES_MASTER_NODE / 2] + 1`

if [ -z $HOST_IP ];then
    HOST_IP="0.0.0.0"
fi

if [[ $SINGLENODE = "true" ]]; then
    cp ./config/single-es.yml ./config/elasticsearch.yml -f
else
    sed -i "s/##NODE_DATA##/$NODE_DATA/g" ./config/elasticsearch.yml
    sed -i "s/##NODE_MASTER##/$NODE_MASTER/g" ./config/elasticsearch.yml
    sed -i "s/##EXPORTER##/$EXPORTER/g" ./config/elasticsearch.yml
    sed -i "s/##UNICAST##/$UNICAST/g" ./config/elasticsearch.yml
    sed -i "s/##ALAUDA_ES_EXPECTED_NODE##/$ALAUDA_ES_EXPECTED_NODE/g" ./config/elasticsearch.yml
    sed -i "s/##RECOVER_AFTER_NODES##/$RECOVER_AFTER_NODES/g" ./config/elasticsearch.yml
    sed -i "s/##MINIMUM_MASTER_NODES##/$MINIMUM_MASTER_NODES/g" ./config/elasticsearch.yml
    sed -i "s/##PASSWORD##/$ALAUDA_ES_PASSWORD/g" ./config/elasticsearch.yml
fi

sed -i "s/ALAUDA_ES_REPLICA/$ALAUDA_ES_REPLICA/g" ./templates/*.json
sed -i "s/ALAUDA_ES_SHARDING/$ALAUDA_ES_SHARDING/g" ./templates/*.json
sed -i "s/{{PASSWORD}}/$ALAUDA_ES_PASSWORD/g" ./after_build/create_mapping.sh
sed -i "s/{{USERNAME}}/$ALAUDA_ES_USERNAME/g" ./after_build/create_mapping.sh
sed -i "s!{{PASSWORD}}!$HASH_ES_PASSWORD!g" ./plugins/search-guard-6/sgconfig/sg_internal_users.yml
sed -i "s/{{USERNAME}}/$ALAUDA_ES_USERNAME/g" ./plugins/search-guard-6/sgconfig/sg_internal_users.yml
sed -i "s/{{USERNAME}}/$ALAUDA_ES_USERNAME/g" ./plugins/search-guard-6/sgconfig/sg_roles_mapping.yml
sed -i "s/{{USERNAME}}/$ALAUDA_ES_USERNAME/g" /root/.curator/curator.yml
sed -i "s!{{PASSWORD}}!$ALAUDA_ES_PASSWORD!g" /root/.curator/curator.yml

sed -i "s/##HOST_IP##/$HOST_IP/g" ./config/elasticsearch.yml

xms=`echo $ES_JAVA_OPTS | awk -F -Xms '{print $2}' | awk '{print $1}'`
xmx=`echo $ES_JAVA_OPTS  | awk -F -Xmx '{print $2}' | awk '{print $1}'`

ulimit -n 65536
chown -R elasticsearch:elasticsearch /esdata
chown -R elasticsearch:elasticsearch .

printenv >> /etc/environment
crond
./sginit.sh & 2>&1

/usr/local/bin/docker-entrypoint.sh eswrapper