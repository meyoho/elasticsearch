FROM docker.elastic.co/elasticsearch/elasticsearch:6.7.1

RUN yum update -y && \
    yum -y install net-tools && \
    yum -y install python curl && \
    yum -y install vixie-cron crontabs

COPY ./search-guard-6-6.7.1-25.4.zip ./
COPY ./prometheus-exporter-6.7.1.0.zip ./
COPY ./elasticsearch-analysis-ik-6.7.1.zip ./

RUN curl https://bootstrap.pypa.io/get-pip.py | python && \
    rm -rf /root/.cache/pip/ && \
    bin/elasticsearch-plugin install -b file:///usr/share/elasticsearch/search-guard-6-6.7.1-25.4.zip && \
    bin/elasticsearch-plugin install -b file:///usr/share/elasticsearch/prometheus-exporter-6.7.1.0.zip && \
    bin/elasticsearch-plugin install -b file:///usr/share/elasticsearch/elasticsearch-analysis-ik-6.7.1.zip

COPY curator/curator.yml /root/.curator/
COPY es/config ./config
COPY templates ./templates
COPY search_guard ./plugins/search-guard-6/

COPY after_build ./after_build
COPY crontab_job ./crontab_job
COPY requirements.txt .
COPY Crontabfile .
RUN mkdir -p ./crontab_job/log
RUN pip install --no-cache-dir -r requirements.txt

COPY Crontabfile /etc/cron.d/cron-file
RUN chmod 0644 /etc/cron.d/cron-file
RUN crontab /etc/cron.d/cron-file

ENV SG_PATH /usr/share/elasticsearch/plugins/search-guard-6

COPY ./sginit.sh ./
COPY ./wrapper.sh ./

RUN chmod +x \
        ${SG_PATH}/tools/hash.sh \
        ${SG_PATH}/tools/sgadmin.sh \
        ./sginit.sh \
        ./wrapper.sh

ENTRYPOINT ["/bin/bash","./wrapper.sh"]

