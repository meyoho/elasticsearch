#!/bin/bash

sleep 20s

${SG_PATH}/tools/sgadmin.sh -ts /usr/share/elasticsearch/config/truststore.jks -tspass alauda_ts -ks ${SG_PATH}/sgconfig/sgadmin-keystore.jks -kspass alauda_ts -cd ${SG_PATH}/sgconfig/ -icl -nhnv

sleep 5s

curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-log -d @./templates/log.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-resources -d @./templates/resources.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_template/alauda-event -d @./templates/event.json

curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_xpack/license -d @./license.json
curl -vX PUT -u {{USERNAME}}:{{PASSWORD}} -H "Content-Type: application/json" localhost:9200/_xpack/license?acknowledge=true -d @./license.json