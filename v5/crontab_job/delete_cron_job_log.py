#!/usr/bin/env python
import datetime
import os

LOG_PATH = '/usr/share/elasticsearch/crontab_job/log'


def main():
    now = datetime.date.today()
    expired_day = now - datetime.timedelta(days=7)
    expired_day_to_str = expired_day.strftime("%Y%m%d")
    for root, dirs, files in os.walk(LOG_PATH):
        for f in files:
            date_to_str = f.split('-')[-1]
            if date_to_str == expired_day_to_str + '.log':
                os.remove(LOG_PATH + '/' + f)


if __name__ == '__main__':
    main()
