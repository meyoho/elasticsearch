#!/bin/bash

#初始化变量默认值
ttl=3d
es_master_ip=nul
es_username="root"
es_passwd="alauda"

#获得密码
if [ -n "$ALAUDA_ES_PASSWORD" ]; then
   es_passwd=$ALAUDA_ES_PASSWORD
fi

echo 'es_username:'$es_username
echo 'es_passwd:'$es_passwd

echo '------------------------'
#获得容器ttl
if [ -n "$ALAUDA_ES_TTL" ]; then
   d_ttl=$ALAUDA_ES_TTL
else
   d_ttl=$ttl
fi
ttl=${d_ttl:0:-1}
echo 'ttl:'$ttl
echo '---------------------------'
#ttl必须大于等于1天
if [ $ttl -lt 1 ];then
   echo 'ttl must be equal or greater than 1 day'
   exit 0
fi

#获得es的master ip
es_master_ip=`eval curl -s -u $es_username:$es_passwd localhost:9200/_cat/master | awk '{print $3}'`
echo 'es_master_ip:'$es_master_ip
echo '------------------------'

# 列出索引
indices_log=`curl -s -u $es_username:$es_passwd $es_master_ip:9200/_cat/indices | grep log | awk '{print $3}'`


#处理日志 哪些保留 哪些删除
##列出日志索引
array_indices=($indices_log)

for i in ${array_indices[@]};
do
   echo 'log_array_indices:'$i
done
echo '------------------'
##从array_indices数组里剔除需要保留的log 剩下的被删除
for (( j=0; j<=$ttl; j++ ))
do
  param="$j days ago"
  tmp=`date -d "$param" +%Y%m%d`
  persist_log="log-"$tmp
  echo 'persist_log:'$persist_log
  for (( i=0; i<=${#array_indices[@]}; i++ ))
  do
    if [ "${array_indices[$i]}" == "$persist_log" ]; then
        array_indices[$i]=0
    fi
  done
done

for k in ${array_indices[*]};
do
  if [ $k != 0 ]; then
        echo 'need delete log:'$k
	curl -XDELETE -u $es_username:$es_passwd $es_master_ip:9200/$k
  fi
done