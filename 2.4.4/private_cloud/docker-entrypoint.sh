#!/bin/bash

set -e

if [ "${1:0:1}" = '-' ]; then
	set -- elasticsearch "$@"
fi

if [ -z $ALAUDA_ES_PASSWORD ];then
    ALAUDA_ES_PASSWORD=alauda
fi

if [ -z $ALAUDA_ES_CLUSTERS ];then
    ALAUDA_ES_CLUSTERS=127.0.0.1
fi

NODE_MASTER="true"
NODE_DATA="true"
if [[ $NODE_MODEL = "master" ]];then
    NODE_DATA="false"
fi

if [[ $NODE_MODEL = "data" ]];then
    NODE_MASTER="false"
fi

if [[ $NODE_MODEL = "none" ]];then
    NODE_MASTER="false"
    NODE_DATA="false"
fi

es_cluster_array=(${ALAUDA_ES_CLUSTERS//,/ })
length=${#es_cluster_array[@]}
EXPORTER=""
UNICAST=""
for ((i=0;i<length;i++))
{
    EXPORTER=${EXPORTER}"",\""root:$ALAUDA_ES_PASSWORD@${es_cluster_array[$i]}"\"
    UNICAST=${UNICAST}"",\""${es_cluster_array[i]}"\"
}


EXPORTER=${EXPORTER:1}
UNICAST=${UNICAST:1}

if [ -z $ALAUDA_ES_SHARDING ];then
    ALAUDA_ES_SHARDING=5
fi

if [ -z $ALAUDA_ES_REPLICA ];then
    ALAUDA_ES_REPLICA=1
fi

if [ -z $ALAUDA_ES_EXPECTED_NODE ];then
    ALAUDA_ES_EXPECTED_NODE=2
fi

if [ -z $ALAUDA_ES_MASTER_NODE ];then
    ALAUDA_ES_MASTER_NODE=$length
fi


RECOVER_AFTER_NODES=$[$[$ALAUDA_ES_EXPECTED_NODE * 2] / 3]

if [ $RECOVER_AFTER_NODES -eq 0 ];then
    RECOVER_AFTER_NODES=1
fi

MINIMUM_MASTER_NODES=`expr $[$ALAUDA_ES_MASTER_NODE / 2] + 1`

sed -i "s/{{NODE_DATA}}/$NODE_DATA/g" /es/config/elasticsearch.yml
sed -i "s/{{NODE_MASTER}}/$NODE_MASTER/g" /es/config/elasticsearch.yml
sed -i "s/{{PASSWORD}}/$ALAUDA_ES_PASSWORD/g" /es/config/elasticsearch.yml
sed -i "s/{{PASSWORD}}/$ALAUDA_ES_PASSWORD/g" /kibana/config/kibana.yml
sed -i "s/{{PASSWORD}}/$ALAUDA_ES_PASSWORD/g" ./after_build/create_auth.py
sed -i "s/{{EXPORTER}}/$EXPORTER/g" /es/config/elasticsearch.yml
sed -i "s/{{UNICAST}}/$UNICAST/g" /es/config/elasticsearch.yml
sed -i "s/{{ALAUDA_ES_EXPECTED_NODE}}/$ALAUDA_ES_EXPECTED_NODE/g" /es/config/elasticsearch.yml
sed -i "s/{{RECOVER_AFTER_NODES}}/$RECOVER_AFTER_NODES/g" /es/config/elasticsearch.yml
sed -i "s/{{MINIMUM_MASTER_NODES}}/$MINIMUM_MASTER_NODES/g" /es/config/elasticsearch.yml

sed -i "s/{{PASSWORD}}/$ALAUDA_ES_PASSWORD/g" ./after_build/create_mapping.sh

sed -i "s/ALAUDA_ES_REPLICA/$ALAUDA_ES_REPLICA/g" /es/config/templates/*.json

sed -i "s/ALAUDA_ES_SHARDING/$ALAUDA_ES_SHARDING/g" /es/config/templates/*.json

chown -R elasticsearch:elasticsearch /esdata
chown -R elasticsearch:elasticsearch /es/config
chown -R elasticsearch:elasticsearch .

printenv >> /etc/environment
cron

set -- gosu elasticsearch "$@"

/kibana/bin/kibana &

after_build/create_mapping.sh & 2>&1

exec "$@"
