#!/bin/bash

sleep 30s

curl -vX PUT -u root:{{PASSWORD}} localhost:9200/_template/alauda-log -d @/es/config/templates/log.json
curl -vX PUT -u root:{{PASSWORD}} localhost:9200/_template/alauda-resources -d @/es/config/templates/resources.json
curl -vX PUT -u root:{{PASSWORD}} localhost:9200/_template/alauda-event -d @/es/config/templates/event.json

curl -vX PUT -u root:{{PASSWORD}} localhost:9200/_license -d @/alauda/license.json
curl -vX PUT -u root:{{PASSWORD}} localhost:9200/_license?acknowledge=true -d @/alauda/license.json