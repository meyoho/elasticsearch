#!/usr/bin/env python
import sys
import requests


def main():
    endpoint = 'http://localhost:9200'
    username = sys.argv[1]
    password = sys.argv[2]
    endpoint = endpoint + '/_httpuserauth'
    root_password = '{{PASSWORD}}'
    mode = "adduser"
    payload = {
        'mode': mode,
        'username': username,
        'password': password
    }
    resp = requests.get(endpoint, auth=('root', root_password), params=payload)
    if 200 <= resp.status_code <= 300:
        print "create user succ..."
        mode = 'addindex'
        index = 'log-*,_*,event-*,resource-*'
        payload = {
            'mode': mode,
            'username': username,
            'password': password,
            'index': index
        }
        resp = requests.get(endpoint, auth=('root', root_password), params=payload)
        if 200 <= resp.status_code <= 300:
            print "add index succ..."
            print "all ok !"
        else:
            print resp.text
    else:
        print resp.text


if __name__ == '__main__':
    main()
